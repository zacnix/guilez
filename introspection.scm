#!/usr/bin/guile -s \

!#

(define-module (helpers introspection)
  #:export (current-source-filename))

(define-syntax current-source-filename
  (syntax-rules ()
    ((_)
     (search-path %load-path (cdr (assoc 'filename (current-source-location)))))))
