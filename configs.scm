#!/usr/bin/guile \
-s
!#


;(add-to-load-path "/home/zharvey/Documents/code/libs")

(define-module (helpers configs)
  #:use-module (ice-9 receive)
  #:use-module ((helpers log log) #:prefix log:)
  #:use-module (helpers cmd)
  #:export (run-template get-jinja2-cmd))

(define vars "testing-configs.yml")

(define* (get-jinja2-cmd template vars outfile #:key (format "yaml"))
  (list
   "jinja2"
   "--strict"
   (string-append "--format=" format)
   (string-append "--outfile=" outfile)
   template
   vars))

(define (to-project-home repo path)
  (if (string-suffix? "/" path 0 1 0 1)
      path
      (string-append (repo-path repo) "/" path)))

(define* (run-template template vars #:key (tmp ""))
  (let* ((tmp (if (string-null? tmp) (tmpnam) tmp))
         (cmd (list->command (get-jinja2-cmd template vars tmp)))
         (exit (execute-cmd->command cmd)))
    (log:debug "Exiting from jinja2 " exit)
    (values tmp exit)))
