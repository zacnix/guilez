#!/usr/bin/guile3.0 \
-s
!#

(define-module (helpers user-input)
  #:use-module (helpers alist)
  #:use-module ((helpers log log) #:prefix log:)
  #:use-module (ice-9 readline)
  #:export (user-entry-operation
            read-user-value
            curry-read-user-value
            user-entry-completion
            get-user-values))

(define* (read-user-value key #:optional (default #f) (default-val #f))
  (let ((val (readline
              (format #f "Enter value for ~A ~A: "
                      key
                      (if default
                          (format #f "[~A]" default-val)
                          "")))))
    (if (not (string=? "" val))
        (string-trim-both val)
        (if default
            default-val
            (read-user-value key default default-val)))))

(define* (curry-read-user-value #:optional (default #t))
    (lambda (default-val key) (read-user-value key default default-val)))

(define* (get-user-values keys #:optional (default #f) (default-alst #f))
  (let ((func (curry-read-user-value (not (null? default-alst)))))
    (let f ((vals '())
            (k keys))
      ;(log:debug k (assoc-ref default-alst (car k)) default-alst)
      (if (null? k)
          vals
          (f (acons (car k) (func (assoc-ref default-alst (car k)) (car k)) vals) (cdr k))))))

(define* (user-entry-operation alst #:optional (default #f) (default-vals #f))
  (log:debug "user-entry-operation " alst default)
  (let* ((answers (get-user-values (get-keys alst) default default-vals)))
    ;(log:info answers)
    answers))

(define* (user-entry-completion completes key #:optional (default #f) (default-val #f))
  (with-readline-completion-function
   (make-completion-function completes)
   (lambda () (read-user-value key default default-val))))

