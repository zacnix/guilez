#!/usr/bin/guile

!#

(define-module (helpers pretty)
  #:use-module (helpers print))

(pretty-file (cadr (command-line)))
