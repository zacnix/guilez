

(define-module (helpers gnuargs)
  #:use-module (ice-9 receive)
  #:use-module (helpers argparse)
  #:use-module ((helpers log log) #:prefix log:)
  #:export (parse-gnu-option
            parse-long-gnu-option
            parse-short-gnu-option
            to-tokens
            parse-options
            parse-gnu-option))

(define LONG-DEL "--")
(define SHORT-DEL "-")
;; https://www.gnu.org/software/libc/manual/html_node/Argument-Syntax.html

;(define (parse-options args)
;  (if (null? args)
;      '()
;      (list (parse-gnu-option (car args)) (parse-options (cdr args)))))

(define (parse-options all-args)
  (let next ((args all-args) (objs '()))
       (if (null? args)
           objs
           (cond
            ((string=? SHORT-DEL (car args)) (next (cdr args) (append objs (list (make-argument (car args) "" "")))))
            ((string=? LONG-DEL (car args)) (append objs (list (objects->argument args))))
            ((string-prefix? LONG-DEL (car args))
               (next (cdr args) (append objs (list (parse-long-gnu-option (car args))))))
            ((string-prefix? SHORT-DEL (car args))
             (next (cdr args) (append objs (parse-short-gnu-option (car args)))))
            (else
             (next (cdr args) (append objs (list (parse-gnu-option (car args))))))))))

(define (parse-gnu-option arg)
  "This will take in a string of arg. If this string matches a pattern of --anything or -anything it will be
split by the hypens and packed in an <argument> and returned. If neither is matched then the whole
string will be packed in the word part of <argument> and returned."
  (cond
   ((string-prefix? LONG-DEL arg) (parse-long-gnu-option arg))
   ((string-prefix? SHORT-DEL arg) (parse-short-gnu-option arg))
   (else (init-argument arg ""))))
#!
(define (parse-long-gnu-option argument)
  "This will take in a long option of type <argument> and then
it will then split it by the first equals sign and return the
string on the right. If no equals is found than it will return #f"
  (let ((word (argument-word argument))
        (option-pos (string-contains word "=")))
    (if option-pos
        (string-drop word option-pos)
        #f)))
!#

(define* (parse-long-gnu-option argument #:optional (prefix-delimiter LONG-DEL) (value-split "="))
  "Take in a string as argument that matches --XXXX or
--XXXX=YYYY and break it apart into <argument> and value"
  (let* ((full-option
          (if (string-prefix? prefix-delimiter argument)
              (string-drop argument (string-length prefix-delimiter))
              argument))
         (value-pos (string-contains full-option value-split))
         (value (if value-pos
                    (string-drop full-option (+ (string-length value-split) value-pos))
                    ""))
         (word (if value-pos
                   (string-drop-right full-option (- (string-length full-option) value-pos))
                   full-option)))
    (make-argument word prefix-delimiter value)))

(define* (peak-option argument #:optional (prefix-char #\-) (long #f))
  "This will pull off the prefix and return rest of the characters
if long is true and the first character if not."
  (let ((trimmed (string-trim argument prefix-char)))
    (if long
        trimmed
        (string-take trimmed 1))))

(define* (parse-short-gnu-option-with-value argument #:optional (prefix-str SHORT-DEL))
  "Take in a string as argument and convert it from a -acat to
<argument a> <argument cat>. Where 'a' is the option and cat is
the value of the option."
  (let* ((option (string-drop argument (string-length prefix-str))))
    (make-argument option prefix-str (string-drop option 1))))

(define* (parse-short-gnu-option argument #:optional (pull-value #f) (prefix-str SHORT-DEL))
  (if pull-value
      ; We are gonna pack this into a list just so that the return value is always a list regardless of which function call
      (list (parse-short-gnu-option-with-value argument prefix-str))
      (parse-short-gnu-options argument prefix-str)))

(define* (parse-short-gnu-options argument #:optional (prefix-str SHORT-DEL))
  "Take in a string as argument and convert it from
a -a or -abcd to a list of arguments
Return Value : (<argument> ...)"
  (let loop ((word (string->list
                    (if (string-prefix? prefix-str argument)
                        (string-drop argument (string-length prefix-str)) ; Remove the beginning -
                        argument)))
             (args '()))
    (if (null? word)
        args
        (loop (cdr word) (append args (list (init-argument (list->string (list (car word))) prefix-str)))))))

(define (to-tokens cmd-line)
  "Take in a cmd-line argument that is a list of strings, same as what
comes out of (command-line) function. Then we will turn these into a list
of <argument> objects"
  (if (null? cmd-line)
      '()
      (cons (parse-gnu-option (car cmd-line)) (to-tokens (cdr cmd-line)))))
