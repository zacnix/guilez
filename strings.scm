#!/usr/bin/guile3.0 \
-s
!#

(define-module (helpers strings)
  #:export (list-syms->list-strings
            string-postfix?))


(define (list-syms->list-strings list)
  (if (null? list)
      '()
      (cons
       (symbol->string (car list))
       (list-syms->list-strings (cdr list)))))

(define (string-postfix? s1 s2)
  (string-prefix? (string-reverse s1) (string-reverse s2)))

(define (string-postfix-ci? s1 s2)
  (string-prefix-ci? (string-reverse s1) (string-reverse s2)))
