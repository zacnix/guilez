#!/usr/bin/guile3.0

!#

(define-module (helpers oops)
  #:use-module (oop goops)
  #:use-module (helpers print)
  #:export (get-goops-method-info get-goops-generic-info))


;(define* (get-instance-methods instance) (get-class-methods (class-of instance)))

;(define* (get-class-methods klass))

(define (get-goops-method-info method)
  (if (is-a? method <method>)
      (get-goops-generic-info (method-generic-function method))))

(define (get-goops-generic-info generic)
  (if (is-a? generic <generic>)
      (map (lambda (m)
             (print
              (generic-function-name (method-generic-function m))
              " : "
              (method-formals m)))
           (generic-function-methods generic)
      )))
