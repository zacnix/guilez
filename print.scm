(define-module (helpers print)
  #:use-module (ice-9 pretty-print)
  #:use-module (helpers record-introspection)
  #:export (print pretty-obj remove-keys pretty-file))

(define (remove-keys lst keys)
  (define (a l)
    (cond ((= 0 (length l)) l)
      (else
       (let ((iskey (member (car l) keys)))
         (cond ((not iskey)
            (cond ((= 1 (length l)) l)
              (else (cons (car l) (a (cdr l))))))
           (else
            (cond ((= 1 (length l)) '())
              (else (a (cddr l))))))))))
  (a lst))

(define* (print #:key (sep " ") (end "\n") (port 'stdout) (flush #f) #:rest vals)
  ;(display vals) (display "\n")
  (define (b v)
    (let ((len (length v)))
      (cond ((< 1 len) (display (car v)) (display sep) (b (cdr v)))
        ((= 1 len) (display (car v)) (display end))
        (else (display end)))))
  (b (remove-keys vals '(#:sep #:end #:port #:flush))))

(define* (pretty-obj obj #:optional (fix-str? #t))
  (pretty-print (object->writeable obj)))


(define (pretty-file file)
  (call-with-input-file file
    (lambda (port)
      (let f ((obj (read port)))
        (if (not (eof-object? obj))
            (begin (pretty-print obj) (f (read port))))))))
