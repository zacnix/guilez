(define-module (helpers file-ops)
  #:use-module (ice-9 ftw)
  #:use-module (ice-9 receive)
  #:use-module (helpers philez)
  #:use-module ((helpers log log) #:prefix log:)
  #:export (mkdir-parents
            copy-dir-tree
            copy-file-tree
            total-file-size
            makedirs
            split-extension))

(define* (mkdir-parents file-path-str #:optional (root ""))
  (let lp ((split (string-split file-path-str #\/))
           (path root))
    (unless (null? split)
      (let ((mk-path (string-append (if (string= "" path) "" (string-append path file-name-separator-string)) (car split))))
        (unless (file-exists? mk-path)
          (log:debug "mkdir " mk-path)
          (mkdir mk-path))
        (lp (cdr split) mk-path)))))

;(define (copy-tree 

(define (copy-dir-tree dir dst)
  (log:debug "copy-dir-tree" dir dst)
  (let ((dir-stream (opendir dir)))
    (let lp ((d (readdir dir-stream)))
      (if (eof-object? d)
          #t
          (let ((full-path (string-append dir file-name-separator-string d)))
            (cond ((or (equal? ".." d) (equal? "." d))
                   (lp (readdir dir-stream)))
                  ((is-dir? full-path)
                   (let ((new-dir full-path))
                     (log:debug "(is-dir? full-path)" (is-dir? full-path))
                     (makedirs new-dir)
                     (copy-dir-tree full-path new-dir)
                     (lp (readdir dir-stream))))
                  (#t
                   (copy-file full-path (string-append dst file-name-separator-string d))
                   (log:debug "copy-file " full-path dst)
                   (lp (readdir dir-stream)))))))))


(define* (makedirs dir #:optional (exists-ok #t))
  (with-exception-handler
      (lambda (err)
        (let ((errno (system-error-errno (append (list (exception-kind err)) (exception-args err)))))
          (if (and exists-ok (eq? errno EEXIST))
              #t
              (raise-exception err #t))))
    (lambda () (mkdir dir)) #:unwind? #t))

(define (total-file-size file-name)
  "Return the size in bytes of the files under FILE-NAME (similar
to `du --apparent-size' with GNU Coreutils.)"

  (define (enter? name stat result)
    (log:debug "enter? " name stat result)
    ;; Skip version control directories.
    (not (member (basename name) '(".git" ".svn" "CVS"))))
  (define (leaf name stat result)
    (log:debug "leaf " name stat result)
    ;; Return RESULT plus the size of the file at NAME.
    (+ result (stat:size stat)))

  ;; Count zero bytes for directories.
  (define (down name stat result)

    (log:debug "down " name stat result)
    result)
  (define (up name stat result)
        (log:debug "up " name stat result)

    result)

  ;; Likewise for skipped directories.
  (define (skip name stat result)     (log:debug "skip " name stat result)
result)

  ;; Ignore unreadable files/directories but warn the user.
  (define (error name stat errno result)
    (format (current-error-port) "warning: ~a: ~a~%"
            name (strerror errno))
    result)

  (file-system-fold enter? leaf down up skip error
                           0  ; initial counter is zero bytes
                           file-name))

(define (copy-file-tree dir)
  (define (enter? name stat result)
    #t)
  (define (leaf name stat result)
    (log:debug "Copy name" name)
    #t)
  (define (up path stat result)
    #t)
  (define (down path stat result)
    (log:debug "mkdir path" path)
    #t)
  (define (skip name stat result)
    (log:debug "skipping name " name) #t)
  (define (error name stat result)
    (log:debug "Error on name " name) #t)
  (file-system-fold enter? leaf down up skip error 0 dir))


(define* (split-extension filename #:optional (ext-char #\.))
  (let* ((rind (string-rindex filename ext-char)))
    (if rind
        (values (string-take filename rind) (string-take-right filename (- (string-length filename) rind)))
        (values filename ""))))
