

(define-module (helpers date)
  #:use-module (srfi srfi-19)
  #:use-module ((srfi srfi-18) #:prefix time:)
  #:export (get-utc-formated-string))

(define iso8601-no-seps "~Y~m~dT~H~M~S~z")
(define (get-utc-formated-string)
  (date->string (current-date 0) iso8601-no-seps))

