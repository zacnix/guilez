(define-module (helpers record-introspection)
  #:use-module ((rnrs records inspection) #:prefix rnrs:)
  #:use-module ((srfi srfi-9) #:prefix srfi:)
  #:use-module (ice-9 pretty-print)
  #:use-module ((helpers log log) #:prefix log:)
  #:use-module ((oop goops) #:prefix oop:)
  #:export (record->alist object->writeable pretty-record))

                                        ; Look into module (ice-9 session) to get procedure information
; This should make it possible to create a record from an alist we would just need the record type
(define (get-record-fields obj)
  (record-type-fields (rnrs:record-rtd obj)))

(define (record->alist obj)
  (let f ((alst '())
          (fields (get-record-fields obj)))
    (if (null? fields)
        alst
        (f
         (assoc-set!
          alst
          (car fields)
          (object->writeable ((record-accessor
            (rnrs:record-rtd obj)
            (car fields)) obj)))
         (cdr fields)))))

(define (object->writeable obj)
  ;(log:debug "object->writeable writing obj" obj)
  (cond
   ((record? obj) (record->alist obj))
   ((list? obj) (begin
                  (let f ((out '()) (ins obj))
                    (if (null? ins)
                        out ; (begin (log:debug "object->writeable returning " out) out)
                        (f (append out (list (object->writeable (car ins)))) (cdr ins))))))
   (else obj)))

(define (pretty-record rec)
  (pretty-print (record->alist rec)))


;(define (record->alist obj alist)
;  (let f ((alst '())
;          (fields (get-record-fields obj)))
;    (if (null? fields)
;        #t
;        (f
;         (assoc-set!
;          alst
;          (car fields)
;          (object->writeable ((record-accessor
;            (rnrs:record-rtd obj)
;            (car fields)) obj)))
;         (cdr fields)))))

