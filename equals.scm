(define-module (helpers equals)
  #:use-module (helpers alist)
  #:use-module (helpers record-introspection)
  #:use-module (srfi srfi-1)
  #:use-module ((rnrs records inspection) #:prefix rnrs:)
  #:use-module ((rnrs records procedural) #:prefix rnrs:)
  #:use-module ((helpers log log) #:prefix log:)
  #:export (lists-same? deep-compare-alist record-eq-alist lists-contents-same?))


(define* (lists-same? left right #:optional (comparator eqv?))
  (if (eqv? (length left) (length right))
      (begin
        (let f ((l left) (r right))
          (cond
           ((null? l) #t)
           ((comparator (car l) (car r)) (f (cdr l) (cdr r)))
           (else #f))))
      #f))

(define* (lists-contents-same? left right #:optional (comparator eqv?))
  "Compare two lists of left and right to see if their contents are the same regardless of order"
  (cond
   ((not (eqv? (length left) (length right))) #f)
   ((equal? left right) #t)
   (else
    (let loop ((left left) (right right))
      (if (not (null? left))
          (let ((head (car left)))
            (if (member head right comparator)
                (loop (cdr left) (delete (car left) right))
                #f))
          #t)))))


(define* (deep-compare-alist left right #:optional (comparator eqv?))
  (let* ((left-keys (get-keys left))
         (same-keys (lists-same? left-keys (get-keys right))))
    (if (not same-keys)
        #f
        (begin
          (let f ((keys left-keys))
            (cond
             ((null? keys) #t)
             ((comparator (assoc-ref left (car keys)) (assoc-ref right (car keys))) (f (cdr keys)))
             (else #f)))))))

(define* (record-eq-alist obj alist #:optional (comparator equal?))
  (let ((fields (record-type-fields (rnrs:record-rtd obj)))
        (rtd (rnrs:record-rtd obj)))
    (if (equal? fields (get-keys alist))
        (let f ((check fields))
          (cond
           ((null? check) #t)
           ((comparator (assoc-ref alist (car check)) ((rnrs:record-accessor rtd (car check)) obj)) (f (cdr check)))
           (else (begin (log:debug "Failing on key " (car check))#f))))
        (begin (log:debug "Failing on fields equal" fields (get-keys alist)) #f))))
