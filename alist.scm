#!/usr/bin/guile3.0 \
-s
!#

(define-module (helpers alist)
  #:export (get-keys
            get-values
            assoc-default
            zip->alist
            alist->yaml))

(define (get-keys alist)
  (let f ((alst alist) (keys '()))
    (if (null? alst)
        keys
        (f (cdr alst) (append (list (caar alst)) keys)))))

(define (get-values alist)
  (let f ((alst alist) (vals '()))
    (if (null? alst)
        vals
        (f (cdr alst) (cons (cdar alst) vals)))))

(define (assoc-default alst key default)
  (let ((val (assoc-ref alst key)))
    (if val val default)))


(define (zip->alist keys vals)
  "Create an alist matching keys to vals.
((keys[0] . vals[0]) (keys[1] . vals[1])...)
It will stop once either keys or vals is ()"
  (if (or (null? keys) (null? vals))
      '()
      (append (list (cons (car keys) (car vals))) (zip->alist (cdr keys) (cdr vals)))))


(define (alist->yaml alst)
  (if (null? alst)
      ""
      (format #f "~A: ~A~%~A"
              (if (string? (caar alst)) (caar alst) (symbol->string (caar alst)))
              (cdar alst)
              (alist->yaml (cdr alst)))))
