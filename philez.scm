#!/usr/bin/guile3.0

!#

(define-module (helpers philez)
  #:use-module (srfi srfi-9)
  #:use-module ((helpers log log) #:prefix log:)
  #:export (<phile>
            make-phile
            list-philes
            phile-name
            phile-linkname
            phile-absolute-name
            phile-stat
            phile-filetype
            phile-is-dir?
            print-phile
            is-sock?
            is-fmt?
            is-lnk?
            is-reg?
            is-blk?
            is-dir?
            print
            remove-keys
            is-chr?
            is-fifo?
            join-posix-path
            join-path))

(define (remove-keys lst keys)
  (define (a l)
    (cond ((= 0 (length l)) l)
      (else
       (let ((iskey (member (car l) keys)))
         (cond ((not iskey)
            (cond ((= 1 (length l)) l)
              (else (cons (car l) (a (cdr l))))))
           (else
            (cond ((= 1 (length l)) '())
              (else (a (cddr l))))))))))
  (a lst))


(define* (join-posix-path path #:rest pieces)
  (if (null? pieces)
      path
      (apply
       join-posix-path
       (cons (string-append path file-name-separator-string (car pieces)) (cdr pieces)))))

(define* (join-path path #:rest pieces)
  (apply join-posix-path (cons path pieces)))


(define* (print #:key (sep " ") (end "\n") (port 'stdout) (flush #f) #:rest vals)
  ;(display vals) (display "\n")
  (define (b v)
    (let ((len (length v)))
      (cond ((< 1 len) (display (car v)) (display sep) (b (cdr v)))
        ((= 1 len) (display (car v)) (display end))
        (else (display end)))))
  (b (remove-keys vals '(#:sep #:end #:port #:flush))))


; 'regular 'directory 'symlink 'block-special 'char-special 'fifo 'socket 'unknown
; 'regular 'directory 'symlink 'block-special 'char-special 'fifo 'socket 'unknown
(define S_IFMT #o0170000)
(define (is-fmt? mask check)
  (eq? (logand S_IFMT check) mask))

(define (make-stat-mode check)
  (cond ((number? check)
         check)
        ((string? check)
         (stat:mode (stat check)))))

(define S_IFSOCK #o0140000)
(define (is-sock? check)
  (is-fmt? S_IFSOCK (make-stat-mode check)))

(define S_IFLNK #o0120000)
(define (is-lnk? check)
  (is-fmt? S_IFLNK (make-stat-mode check)))

(define S_IFREG #o0100000)
(define (is-reg? check)
  (is-fmt? S_IFREG (make-stat-mode check)))

(define S_IFBLK #o0060000)
(define (is-blk? check)
  (is-fmt? S_IFBLK (make-stat-mode check)))

(define S_IFDIR #o0040000)
(define (is-dir? check)
  (is-fmt? S_IFDIR (make-stat-mode check)))

(define S_IFCHR #o0020000)
(define (is-chr? check)
  (is-fmt? S_IFCHR (make-stat-mode check)))

(define S_IFIFO #o0010000)
(define (is-fifo? check)
  (is-fmt? S_IFIFO (make-stat-mode check)))



(define-record-type <phile>
  (phile name linkname absolute-name stat filetype)
  phile?
  (name phile-name)
  (linkname phile-linkname)
  (absolute-name phile-absolute-name)
  (stat phile-stat)
  (filetype phile-filetype))

(define (phile-is-dir? phile-obj)
  (eq? (phile-filetype phile-obj) 'directory))

(define (make-phile phile-str)
  (let* ((base (if (absolute-file-name? phile-str) (basename phile-str) phile-str))
         (st (lstat phile-str))
         (abs (if (eq? 'symlink (stat:type st)) phile-str (canonicalize-path phile-str))))
    ;(display "philing string ") (display phile-str)(newline)
    (phile base abs abs st (stat:type st))))

(define (list-philes dir)
  (let ((iter (opendir dir)))
    (let lp ((nxt (readdir iter)))
      (if (eof-object? nxt)
          '()
          (append
           (list
            (with-exception-handler
                (lambda (condition-or-value)
                  (log:error "Failed to stat " nxt)
                  #nil)
              (lambda ()
                (make-phile nxt))
              #:unwind? #t))
           (lp (readdir iter)))))))

(define (print-all dir)
  (let ((iter (opendir dir)))
    (let lp ((nxt (readdir iter)))
      (if (not (eof-object? nxt))
          (begin
            (display nxt)
            (newline)
            (lp (readdir iter)))))))

(define (print-phile ph)
  (print "name " (phile-name ph)
         "\nlinkname " (phile-linkname ph)
         "\nabsolute-name " (phile-absolute-name ph)
         "\nstat " (phile-stat ph)
         "\nfiletype " (phile-filetype ph)))

;(print-all "/home/zac")
