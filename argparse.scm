#!/usr/bin/guile \
-s
!#

(define-module (helpers argparse)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-19)
  #:use-module (ice-9 receive)
  #:use-module (ice-9 exceptions)
                                        ;#:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module ((helpers log log) #:prefix log:)
  #:export (make-argument
            init-argument
            argument?
            argument-word
            argument-prefix
            argument=?
            argument-value
            objects->argument
            to-tokens
            string->argument
            find-commands-of-prefix
            sort-commands-by-prefix
            get-all-prefixs
            match-string-to-command
                                        ;to-commands
            init-command
            command-full-name
            command-short-name
            command-help-message
            command-prefix
            command-operator
            ))

  ; This is an object descirbing a passable command line option
(define-immutable-record-type <command>
  (make-command full-name short-name help-message prefix-char operator)
  command?
  (full-name command-full-name)       ; This should be the full string that a user would enter
  (short-name command-short-name)     ; This would be the short string, such as, --verbose being the full and -V being the short
  (help-message command-help-message) ; If the user asks for descriptions then the system will print this out
  (prefix-char command-prefix)        ; The prefix character the user puts in front of full or short
  (operator command-operator))        ; This would be the function to call against this command appearing

(define* (init-command #:key (full-name "") (short-name "") (help-message "") (prefix-char #f) (operator #f))
  (make-command full-name short-name help-message prefix-char operator))

; This represents what was passed in by the user
(define-immutable-record-type <argument>
  (make-argument word prefix-str value)
  argument?
  (word argument-word)          ; This is the string after the prefix
  (prefix-str argument-prefix) ; This is the prefix string that was passed in
  (value argument-value)) ; If the argument has a value it's stored here

(define* (init-argument word #:optional (prefix-str "") (value ""))
  (make-argument word prefix-str value))

(define (objects->argument args)
  "Pass in a list of objects and you will get an
<argument> for each object being
#<<argument> word: obj prefix-str: \"\">
This is useful for when you run into a -- and know
the rest of the arguments are just values."
  (if (null? args)
      '()
      (cons (make-argument (car args) "" "") (objects->argument (cdr args)))))

(define (compare-prefixes left right)
  "Take two chars and run char<? on them.
If left is not a char or #f then a #f is returned.
If right is not a char or #f then a #t is returned.
If left or right is a <command> then it'll get the prefix
and compare that.
left : char or <command>
right : char or <command>
Return : bool"
  (cond
   ((command? left) (compare-prefixes (command-prefix left) right))
   ((command? right) (compare-prefixes left (command-prefix right)))
   ((not (char? left)) #f)
   ((not (char? right)) #t)
   (else (char<? left right))))

(define (match-prefix chr command)
  "Check if chr is in the command prefix.
chr : char
command : <command> where (command-prefix command) is a list of char
Return : list where car is the matching prefix or ()
Throws : Will throw an exception is command-prefix is not the right type"
  (let ((prefixes (command-prefix command)))
    (cond
     ((list? prefixes)
      (member chr prefixes))
     ((char? prefixes)
      (char=? chr prefixes))
     (else (raise-exception chr #:continuable? #t)))))

(define max-char #x10FFFF)
(define (sort-commands-by-prefix commands)
  (sort commands compare-prefixes))

(define (find-commands-of-prefix commands prefix)
  "Take in a list of command? matching objects and a char? prefix.
We will loop through and get all commands that match the passed in
prefix.
commands : list of command? matching objects
prefix : char
Return : list of commands or ()"
  (let ((sorted (sort-commands-by-prefix commands)))
    (let lp ((rest sorted))
      (cond
       ((null? rest) '())
       ((char=? (command-prefix (car rest)) prefix)
        (let chew ((matching '()) (cmds rest))
          (if (null? cmds)
              matching
              (if (char=? (command-prefix (car cmds)) prefix)
                  (chew (append matching (list (car cmds))) (cdr cmds))
                  matching))))
       (else (lp (cdr rest)))))))

(define* (command->argument cmd #:optional (long #t))
  "This will create an argument that cmd is expected to match.
This does assume a GNU style of - and -- for short and long respectively.
cmd : <command>
long : bool
Return : <argument>"
  (if long
      (init-argument (command-full-name cmd) (list->string (list (command-prefix cmd) (command-prefix cmd))))
      (init-argument (command-short-name cmd) (list->string (list (command-prefix cmd))))))

(define (argument=? left right)
  (and (string=? (argument-word left) (argument-word right))
       ((if (string? (argument-prefix left)) string=? char=?)
        (argument-prefix left)
        (argument-prefix right))))

;(define (check-prefix-char arg command)
;  "This will take an argument as a string and compare"
;  (cond
;   ((string=? (argument-prefix arg) (command-prefix command))
;   ((string=? (command-full-name command) (argument-word arg))
;    (if (string=? (command-prefix command) (argument-prefix arg)))))))

(define (string->argument str prefix)
  "Take a string and a char prefix. If the first char in str matches prefix
will return the str split to prefix and word. The prefix on str can be
any number of characters."
  (if (or (null? prefix) (and (string? prefix) (string=? prefix "")))
      (init-argument str)
      (let ((chars (string->list str))
            (pref (if (char? prefix) prefix (car (string->list prefix)))))
        (if (not (char=? (car chars) pref))
            #f
            (let lp ((prefix-str '()) (all-chars chars))
              (if (not (char=? pref (car all-chars)))
                  (init-argument (list->string all-chars) (list->string prefix-str))
                  (lp (append prefix-str (list (car all-chars))) (cdr all-chars))))))))

(define (find-prefix->argument str prefixs)
  "Loop through all the prefixs and find one that str starts with.
Each prefix is passed to string->argument until a match is found.
If no match is found than #f"
  (if (null? prefixs)
      #f
      (let ((arg (string->argument str (car prefixs))))
        (if arg
            arg
            (find-prefix->argument str (cdr prefixs))))))

(define (argument-match-command? arg command) ; Just use the match-string-to-command
  "This will test arg, being a string, will match the prefix and full or short name
of command, being true from command?, and either return the command of #f"
  (let ((argument (string->argument arg (command-prefix command))))
    (if (not argument)
        #f
        (if (or (string=? (argument-word argument) (command-full-name command))
                (string=? (argument-word argument) (command-short-name command)))
            command
            #f))))

(define (get-all-prefixs commands)
  "Take a list of <command> and find all the prefixs
commands : list of command? matching object
Return : list of chars"
  (let lp ((prefixs '()) (cmd commands))
    (if (null? cmd)
        prefixs
        (if (or (not (command-prefix (car cmd))) (not (char? (command-prefix (car cmd))))
                (find (lambda (pref)
                         (char=? pref (command-prefix (car cmd))))
                      prefixs))
            (lp prefixs (cdr cmd))
            (lp (append prefixs (list (command-prefix (car cmd)))) (cdr cmd))))))

;(define (argument-to-command arg commands)
;  "Take an <argument> and match it to a command"
;  )


(define* (match-string-to-command str command #:optional (ignore-prefix #f))
  "Check if str matches either full or short name in command. Then see if all
the characters before str match command-prefix. So if command has a short name of V
and a prefix of - then a str of -----V will match. But a str of ++++V or --some-V will not.
Like wise for full name. However if ignore-prefix is #t then both -----V and ++++V will match.
The only condition is that all the chars preceeding V are the same.
str : string
command : <command>
Return : #f or <argument>
Throws : Possible from call to match-prefix"
  (let* ((func
         (cond
          ((string-suffix? (command-short-name command) str)
           command-short-name)
          ((string-suffix? (command-full-name command) str)
           command-full-name)
          (else #f))))
    (if (not func)
        #f
        (let* ((len (string-length (func command)))
               (suffix (string-take-right str len))
               (preffix (string-take str (- (string-length str) len)))
               (chr (car (string->list preffix))))
          (if (not (string=? "" (string-trim preffix chr)))
              #f
              (if (or ignore-prefix (match-prefix chr command))
                  (init-argument suffix preffix)
                  #f))))))

(define (to-tokens cmd-line)
  "Take in a cmd-line argument that is a list of strings, same as what
comes out of (command-line) function. Then we will turn these into a list
of <argument> objects"
  (if (null? cmd-line)
      '()
      (cons (string->argument (car cmd-line) "") (to-tokens (cdr cmd-line)))))


;(define (check-prefix-char word char)
;  "This will check if word, as a string, starts with
;char, -, or char char, --, and return the prefixed found and the
;argument without the prefix.
;  word : string
;  char : character
;Return : (string string) or (#f #f)"
;  (if (not (char=? char (car (string->list word))))
;      (values #f #f)
;      (let lp ((prefixs (list (list->string (list char char)) (list->string (list char)))))
;        (if (null? prefixs)
;            (values #f #f)
;            (if (string-prefix? (car prefixs) word)
;                (values (car prefixs) (string-take word (length (car prefixs))))
;                (lp (cdr prefixs)))))))


;(define (match-argument-to-command arg command)
  "This will take arg as a string. The command should be a single object
matching the command? predicate. arg will be tokenized to an <argument> object
using the command as guidance for the prefix. If the arg matches the command
then the command is returned otherwise #f"
;  )
;(define (match-argument-to-command arg commands)
;  (if (null? commands)
;      #f
;      (if (not (command-prefix (car commands)))
;          (if (string=? (argument-word arg) (command-full-name (car commands)))
;              (car commands))
;          (receive (prefix-str word) (check-prefix-char arg (command-prefix (car commands)))
;            (let ((prefix-char (command-prefix (car commands))))
;              (cond
;               ((and (string=? prefix-str (list->string (list prefix-char prefix-char)))
;                     (string=? (command-full-name (car commands))))
;                (car commands))
;               ((and (string=? prefix-str (list->string (list prefix-char prefix-char)))
;                     (string=? (command-short-name (car commands))))
;                (car commands))
;               (else (match-argument-to-command arg (cdr commands)))))))))


;(define (to-commands parsed commands)
;  (let lp ((args parsed) (cmds '()))
;    (if (null? args)
;        cmds
;        (let ((cmd (match-argument-to-command (car args) commands)))
;          (if (not cmd)
;              (lp (cdr args) cmds) ; It should probably raise an exception saying it does not know this argument
;              (lp (cdr args) (append cmds cmd)))))))


;(display (to-tokens '("--long" "poops" "-s" "-q" "--shitz")))
