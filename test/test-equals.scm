(define-module (helpers test test-equals)
  #:use-module (srfi srfi-64)
  #:use-module (test-center center)
  #:use-module (srfi srfi-9)
  #:use-module ((helpers log log) #:prefix log:)
  #:use-module (helpers equals))

(test-injection '(helpers equals))
(test-begin "test-equals")

(define left-list '(one two three four))
(define right-list '(one eight three four))

(define alist-left
  '((one . two)
    (three . "four")
    (five . 6)
    (seven . eight)))

(define key-different-alist
  '((one . two)
    (three . "four")
    (five . 6)
    (key . eight)))

(define value-different-alist
  '((one . two)
    (three . four)
    (five . six)
    (seven . later)))

(define mid-different-alist
  '((one . two)
    (three . mid)
    (five . six)
    (seven . later)))

(define depth-alist
  '((one . two)
    (three . (1 2 3))
    (five . (chicken guile chez))
    (seven . later)))

(define num-different-depth-alist
  '((one . two)
    (three . (1 6 3))
    (five . (chicken guile chez))
    (seven . later)))

(define symbol-different-depth-alist
  '((one . two)
    (three . (1 2 3))
    (five . (chicken lisp chez))
    (seven . later)))

(define-record-type <junk>
  (make-junk one three five seven)
  junk?
  (one junk-one junk-set-one)
  (three junk-three junk-set-three)
  (five junk-five junk-set-five)
  (seven junk-seven junk-set-seven))

(define junk-record
  (make-junk 'two "four" 6 'eight))

(define junk-depth-record
  (make-junk 'two '(1 2 3) '(chicken guile chez) 'later))

(define-test (test-deep-compare-alist)
  (log:debug "test-deep-compare-alist " (current-filename))
  (test-assert "Alists the same" (deep-compare-alist alist-left alist-left))
  (test-not "Keys are different" (deep-compare-alist alist-left key-different-alist))
  (test-not "Values are different at the end of recursion" (deep-compare-alist alist-left value-different-alist))
  (test-not "Values are different half way" (deep-compare-alist alist-left mid-different-alist))
  )

(define-test (test-depth-alist)
  (test-assert "Match ALists with alists in them" (deep-compare-alist depth-alist depth-alist))
  (test-not "Match ALists with alists with different numbers in a list" (deep-compare-alist depth-alist num-different-depth-alist))
  (test-not "Match ALists with alists with different symbols in a list" (deep-compare-alist depth-alist symbol-different-depth-alist)))

(define-test (test-record)
  (test-assert "Match record to alist" (record-eq-alist junk-record alist-left))
  (test-assert "Match record depth" (record-eq-alist junk-record alist-left)))

;(test-deep-compare-alist)
;(test-depth-alist)
;(test-record)

(test-end)
