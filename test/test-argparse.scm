
(define-module (helpers test test-argparse)
  #:use-module (srfi srfi-64)
  #:use-module (srfi srfi-67)
  #:use-module (test-center center)
  #:use-module ((helpers log log) #:prefix log:)
  #:use-module ((helpers argparse) #:prefix args:)
  #:export (commands))


(define (commands)
  (list
   (args:init-command #:full-name "one" #:short-name "o" #:help-message "One argumnets" #:prefix-char #\-)
   (args:init-command #:full-name "two" #:short-name "t" #:help-message "Two argumnets" #:prefix-char #\-)
   (args:init-command #:full-name "three" #:short-name "" #:help-message "Three argumnets")
   (args:init-command #:full-name "four" #:short-name "" #:help-message "Four argumnets" #:prefix-char #\-)
   (args:init-command #:full-name "five" #:short-name "i" #:help-message "Five argumnets" #:prefix-char #\+)
   (args:init-command #:full-name "six" #:short-name "s" #:help-message "Six argumnets" #:prefix-char #\/)))

(define (prefixs) '(#\- #\+ #\/))

(test-injection '(helpers argparse))
;(define 

;(display (args:to-commands '("--two" "-o" "three" "--four") (commands)))

;(define-test (test-argument=?)

(define-test (test-string->argument)
  ;(log:debug (args:string->argument "--two" #\-))
  (test-assert "Find two argument" (args:argument=? (args:make-argument "two" "--") (args:string->argument "--two" #\-)))
  (test-assert "Find short two argument" (args:argument=? (args:make-argument "t" "-") (args:string->argument "-t" #\-)))
  (test-assert "Find plus short argument" (args:argument=? (args:make-argument "o" "+") (args:string->argument "+o" #\+))))

(define-test (test-get-all-prefixs)
  (test-assert "Find all prefixs" (zero? (list-compare (prefixs) (args:get-all-prefixs (commands))))))

(define-test (test-find-commands-of-prefix)
  (test-assert "Find all - dash commands" (args:find-commands-of-prefix (commands) #\-)))

(define-test (test-match-string-to-command)
  (let* ((cmd (car (commands)))
         (pref (args:command-prefix cmd))
         (next-pref (integer->char (+ (char->integer pref) 1)))
         (long (string-append (list->string (list pref pref)) (args:command-full-name cmd)))
         (miss-long (string-append (list->string (list pref next-pref pref)) (args:command-full-name cmd)))
         (diff-prefix-long (string-append (list->string (list next-pref next-pref next-pref next-pref)) (args:command-full-name cmd)))
         (short (string-append (list->string (list pref)) (args:command-short-name cmd))))
    (test-assert "No suffix match on short" (not (args:match-string-to-command "-z" cmd)))
    (test-assert "No suffix match on long" (not (args:match-string-to-command "--sandwich" cmd)))
    (test-assert "Prefix is wrong characters" (not (args:match-string-to-command miss-long cmd)))
    (test-assert "Match on long" (args:match-string-to-command long cmd))
    (test-assert "Match on short" (args:match-string-to-command short cmd))
    (test-assert "Match on long ignore prefix" (args:match-string-to-command diff-prefix-long cmd #t))))

(test-string->argument)
(test-get-all-prefixs)
(test-find-commands-of-prefix)
(test-match-string-to-command)
