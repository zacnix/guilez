#!/usr/bin/guile3.0

!#

(define-module (helpers log log)
  #:use-module (srfi srfi-1)
  #:use-module (helpers print)
  #:use-module (helpers alist)
  #:export (get-color-esc debug info error debug-msg))

(define *log-level* 'debug)

(define log-levels '(debug error warning info everything))

(define *port* 'stdout)

(define *debug-color* 'BLUE)
(define *info-color* 'GREEN)
(define *error-color* 'RED)
(define *warning-color* 'YELLOW)

(define color-codes '(
  (CLEAR   .    "0")
  (RESET   .    "0")
  (BOLD    .    "1")
  (DARK    .    "2")
  (UNDERLINE .  "4")
  (UNDERSCORE . "4")
  (BLINK      . "5")
  (REVERSE    . "6")
  (CONCEALED  . "8")
  (BLACK     . "30")
  (RED       . "31")
  (GREEN     . "32")
  (YELLOW    . "33")
  (BLUE      . "34")
  (MAGENTA   . "35")
  (CYAN      . "36")
  (WHITE     . "37")
  (ON-BLACK  . "40")
  (ON-RED    . "41")
  (ON-GREEN  . "42")
  (ON-YELLOW . "43")
  (ON-BLUE   . "44")
  (ON-MAGENTA . "45")
  (ON-CYAN   . "46")
  (ON-WHITE  . "47")))

(define colors (fold-right (lambda (clr lst) (cons (car clr) lst)) '() color-codes))

(define* (get-color-esc color #:optional (codes color-codes))
  (string-append (string #\esc #\[) (cdr (assoc color codes)) "m"))

(define* (color-print color #:rest vals)
  (apply print (append (list (get-color-esc color)) vals (list (get-color-esc 'CLEAR)))))

(define* (debug-msg #:rest vals)
  (if (eqv? *log-level* 'debug)
      (apply color-print (cons *debug-color* vals))))

(define* (info #:rest vals)
  (apply color-print (cons *info-color* vals)))

(define* (error #:rest vals)
  (apply color-print (cons *error-color* vals)))

(define (current-source-location-str alst)
  (format #f "~A:~A:~A"
          (assoc-default alst 'filename "NIL")
          (assoc-default alst 'line "NIL")
          (assoc-default alst 'column "NIL")))

(define-syntax debug
  (syntax-rules ()
    ((_ msgs ...)
     (debug-msg (current-source-location-str (current-source-location)) msgs ...))))
